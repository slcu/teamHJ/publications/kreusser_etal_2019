## Introduction

This repository links to the MATLAB files used to produce the simulation results 
for the paper 

Kreusser, L., Haskovec, J., Jönsson, H., & Markowich, P. (2019) 
Auxin transport model for leaf venation
https://arxiv.org/abs/1901.03244

The files are hosted on the Cambridge University open data repository:

Kreusser, L., Haskovec, J., Jönsson, H., & Markowich, P. (2019). 
Research data supporting 'Auxin transport model for leaf venation' [Dataset].
https://doi.org/10.17863/CAM.40619

## Running the code

The code for producing the figures is written in MATLAB and has been tested with 
MATLAB R2018a. The main files include all the files whose file names start with 
<i>main_pinmodellingStudy_review</i>. Each file requires a MATLAB function whose 
file name starts with <i>pinmodellingStudy</i>.

For running the main file the following variables in the main file have to be 
adapted accordingly:

<i>savepath</i>: path where output should be saved<br>
<i>textsav</i>: name of the images to be saved<br>
<i>tmax</i>: maximum time which depends on the considered function tax and has to 
be chosen sufficiently large so that a solution close to stationary is obtained<br>
<i>for loop over param</i>: param denotes the variable that is varied in each figure, 
check the figure for the precise values<br>
<i>function</i>: load the appropriate MATLAB function, e.g. for Figure 1: 
<tt>[t,AAux,PPINe] = pinmodellingStudyInitAux(tmax,Nv,Ne,p,Inc,L,Le,param);</tt>


### Main files for the figures:

Figures 1, 2, 4, 5, 6, 7, 8, 9 are created with <i>main_pinmodellingStudy_review.m</i>

Figure 3 (A), (B), (E), (F) are created with <i>main_pinmodellingStudy_review_round.m</i>

Figure 3 (C), (D), (G), (H) are created with <i>main_pinmodellingStudy_review_oval.m</i>

Figure 10 is created with <i>main_pinmodellingStudy_review_multisources.m</i>

Figure 11 is created with <i>main_pinmodellingStudy_review_rectangle.m</i>

### Function files for the figures:

Figure 1: <i>pinmodellingStudyInitAux.m</i><br>
Figure 2: <i>pinmodellingStudySingleSourceStrength.m</i><br>
Figure 3: <i>pinmodellingStudySingleSourceStrength.m</i><br>
Figure 4: <i>pinmodellingStudySingleSinkStrength.m</i><br>
Figure 5: <i>pinmodellingStudyDelta.m</i><br>
Figure 6: <i>pinmodellingStudyTau.m</i><br>
Figure 7: <i>pinmodellingStudyIrregularRandomSelection.m</i><br>
Figure 8: <i>pinmodellingStudyIrregular.m</i><br>
Figure 9: <i>pinmodellingStudyIrregularSinkSources.m</i><br>
Figure 10 (A): <i>pinmodellingStudyIrregularSinkSourcesDistribution.m</i><br> 
Figure 10 (B): <i>pinmodellingStudyIrregularSinkSourcesDistribution2.m</i><br>
Figure 10 (C): <i>pinmodellingStudyIrregularSinkSourcesDistribution3.m</i><br>
Figure 10 (D): <i>pinmodellingStudyIrregularSinkSourcesDistribution4.m</i><br>
Figure 11 (A): <i>pinmodellingStudyIrregularSinkSourcesDistributionRectangle2.m</i><br> 
Figure 11 (B): <i>pinmodellingStudyIrregularSinkSourcesDistributionRectangle3.m</i><br>
Figure 11 (C): <i>pinmodellingStudyIrregularSinkSourcesDistributionRectangle5.m</i><br>
Figure 11 (D): <i>pinmodellingStudyIrregularSinkSourcesDistributionRectangle6.m</i><br>

## Contact:

Main contact for the simulations is Lisa Kreusser <lmk48@cam.ac.uk>.

